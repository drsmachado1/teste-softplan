import java.util.List;
import java.util.stream.Collectors;

public class GeradorObservacao {

	// Textos pr�-definidos
	private String umoNota = "Fatura da nota fiscal de simples remessa: ";
	private String maisDeUmoNota = "Fatura das notas fiscais de simples remessa: ";
	// Identificador da entidade
	private String texto;

	// Gera observa��es, com texto pre-definido, incluindo os n�meros, das notas
	// fiscais, recebidos no par�metro
	public String geraObservacao(List<Integer> numerosNotas) {
		if (!numerosNotas.isEmpty()) {
			return retornaCodigos(numerosNotas) + ".";
		}
		return "";
	}

	// Cria observa��o
	private String retornaCodigos(List<Integer> numerosNotas){
		if (numerosNotas.size() >= 2) {
			texto = maisDeUmoNota;
		} else {
			texto = umoNota;
		}
		return texto + processaLista(numerosNotas);
	}

	private String processaLista(List<Integer> numerosNotas) {
		if(numerosNotas.size() == 1) {
			return numerosNotas.get(0).toString();
		}
		return transformaLista(numerosNotas);
	}

	private String transformaLista(List<Integer> numerosNotas) {
		StringBuffer bufferDeNumerosNotas = new StringBuffer(numerosNotas.stream().map(Object::toString).collect(Collectors.joining(", ")));
		int pos = bufferDeNumerosNotas.lastIndexOf(",");
		bufferDeNumerosNotas = bufferDeNumerosNotas.replace(pos++, pos, " e");
		return bufferDeNumerosNotas.toString();
	}
}