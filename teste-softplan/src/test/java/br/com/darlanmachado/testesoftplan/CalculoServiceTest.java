package br.com.darlanmachado.testesoftplan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.darlanmachado.testesoftplan.domain.CalculoCustoTransporte;
import br.com.darlanmachado.testesoftplan.domain.Carga;
import br.com.darlanmachado.testesoftplan.domain.Veiculo;
import br.com.darlanmachado.testesoftplan.repositories.CargaRepository;
import br.com.darlanmachado.testesoftplan.repositories.VeiculoRepository;
import br.com.darlanmachado.testesoftplan.services.CalculoService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TesteSoftplanApplication.class)
public class CalculoServiceTest {
	private static final String CACAMBA = "Caminhao cacamba";
	private static final String BAU = "Caminhao bau";
	private static final String CARRETA = "Carreta";

	@Autowired
	CalculoService calculoService;
	
	@MockBean
	private CargaRepository cargaRepo;
	
	@MockBean
	private VeiculoRepository veiculoRepo;
	
	@Test
	public void calcularCustoTransporteCaminhaoBauTest() throws Exception {
		Mockito.when(cargaRepo.findAll()).thenReturn(getListaCargas());
		Mockito.when(veiculoRepo.findByTipoVeiculo(Mockito.any())).thenReturn(getVeiculo(BAU));
		validaDados(criaCalculoCustoTransporte(0D, 60D, BAU, 4, 37.2), 
				calculoService.calcularCustoTransporte(criaCalculoCustoTransporte(0D, 60D, BAU, 4, null)));
	}
	
	@Test
	public void calcularCustoTransporteCaminhaoCacambaTest() throws Exception {
		Mockito.when(cargaRepo.findAll()).thenReturn(getListaCargas());
		Mockito.when(veiculoRepo.findByTipoVeiculo(Mockito.any())).thenReturn(getVeiculo(CACAMBA));
		validaDados(criaCalculoCustoTransporte(100D, 0D, CACAMBA, 8, 62.7),
				calculoService.calcularCustoTransporte(criaCalculoCustoTransporte(100D, 0D, CACAMBA, 8, null)));
	}
	
	@Test
	public void calcularCustoTransporteCarretaTest() throws Exception {
		Mockito.when(cargaRepo.findAll()).thenReturn(getListaCargas());
		Mockito.when(veiculoRepo.findByTipoVeiculo(Mockito.any())).thenReturn(getVeiculo(CARRETA));
		validaDados(criaCalculoCustoTransporte(0D, 180D, CARRETA, 12, 150.19), 
				calculoService.calcularCustoTransporte(criaCalculoCustoTransporte(0D, 180D, CARRETA, 12, null)));
	}
	
	private void validaDados(CalculoCustoTransporte esperado, CalculoCustoTransporte recebido) {
		Assert.assertEquals(esperado.getDistanciaRodoviaPavimentada(),recebido.getDistanciaRodoviaPavimentada());
		Assert.assertEquals(esperado.getDistanciaRodoviaNaoPavimentada(), recebido.getDistanciaRodoviaNaoPavimentada());
		Assert.assertEquals(esperado.getVeiculoUtilizado(), recebido.getVeiculoUtilizado());
		Assert.assertEquals(esperado.getPesoCarga(), recebido.getPesoCarga());
		Assert.assertEquals(esperado.getCustoDoTransporte(), recebido.getCustoDoTransporte());
	}
	
	private CalculoCustoTransporte criaCalculoCustoTransporte(Double drp, Double drnp, String tv, Integer peso, Double valor) {
		return new CalculoCustoTransporte(drp, drnp, tv, peso, 
				valor == null ? null : (new BigDecimal(valor).setScale(2, BigDecimal.ROUND_HALF_UP)));
	}

	private Veiculo getVeiculo(String desc) {
		switch(desc) {
		case BAU:
			return new Veiculo(1L, BAU, new BigDecimal(1));
		case CACAMBA:
			return new Veiculo(2L, CACAMBA, new BigDecimal(1.05));
		case CARRETA:
			return new Veiculo(3L, CARRETA, new BigDecimal(1.12));
			default:
				return null;
		}
	}

	private List<Carga> getListaCargas() {
		List<Carga> cargas = new ArrayList<>();
		cargas.add(new Carga(1L, 5, new BigDecimal(0.02)));
		return cargas;
	}
}
