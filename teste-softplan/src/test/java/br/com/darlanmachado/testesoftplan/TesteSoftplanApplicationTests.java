package br.com.darlanmachado.testesoftplan;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import br.com.darlanmachado.testesoftplan.controllers.CalculaCustoTransporteController;
import br.com.darlanmachado.testesoftplan.domain.CalculoCustoTransporte;
import br.com.darlanmachado.testesoftplan.services.CalculoService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CalculaCustoTransporteController.class, secure = false)
public class TesteSoftplanApplicationTests {
	@Autowired
	private MockMvc mock;

	@MockBean
	private CalculoService calculoService;

	private List<String> chamadas;
	private List<String> resultadosEsperados;
	private List<CalculoCustoTransporte> resultados;

	@Before
	public void init() {
		criarChamadas();
		criarResultadosEsperados();
		criaResultados();
	}

	@Test
	public void calculaComDrp100Drnp0VeiculoCacambaPeso8Teste() throws Exception {
		Mockito.when(calculoService.calcularCustoTransporte(Mockito.any())).thenReturn(resultados.get(0));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(chamadas.get(0)).accept(MediaType.APPLICATION_JSON);
		MvcResult result = mock.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		JSONAssert.assertEquals(resultadosEsperados.get(0), result.getResponse().getContentAsString(), false);
	}

	@Test
	public void calculaComDrp0Drnp60VeiculoBauPeso4Teste() throws Exception {
		Mockito.when(calculoService.calcularCustoTransporte(Mockito.any())).thenReturn(resultados.get(1));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(chamadas.get(1)).accept(MediaType.APPLICATION_JSON);
		MvcResult result = mock.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		JSONAssert.assertEquals(resultadosEsperados.get(1), result.getResponse().getContentAsString(), false);
	}

	@Test
	public void calculaComDrp0Drnp180VeiculoCarretaPeso12Teste() throws Exception {
		Mockito.when(calculoService.calcularCustoTransporte(Mockito.any())).thenReturn(resultados.get(2));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(chamadas.get(2)).accept(MediaType.APPLICATION_JSON);
		MvcResult result = mock.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		JSONAssert.assertEquals(resultadosEsperados.get(2), result.getResponse().getContentAsString(), false);
	}

	@Test
	public void calculaComDrp80Drnp20VeiculoBauPeso6Teste() throws Exception {
		Mockito.when(calculoService.calcularCustoTransporte(Mockito.any())).thenReturn(resultados.get(3));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(chamadas.get(3)).accept(MediaType.APPLICATION_JSON);
		MvcResult result = mock.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		JSONAssert.assertEquals(resultadosEsperados.get(3), result.getResponse().getContentAsString(), false);
	}

	@Test
	public void calculaComDrp50Drnp30VeiculoCacambaPeso5Teste() throws Exception {
		Mockito.when(calculoService.calcularCustoTransporte(Mockito.any())).thenReturn(resultados.get(4));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(chamadas.get(4)).accept(MediaType.APPLICATION_JSON);
		MvcResult result = mock.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse().getContentAsString());
		JSONAssert.assertEquals(resultadosEsperados.get(4), result.getResponse().getContentAsString(), false);
	}

	private void criarChamadas() {
		chamadas = new ArrayList<>();
		chamadas.add("/calcula/calcular/100/0/Caminhão caçamba/8");
		chamadas.add("/calcula/calcular/0/60/Caminhão baú/4");
		chamadas.add("/calcula/calcular/0/180/Carreta/12");
		chamadas.add("/calcula/calcular/80/20/Caminhão baú/6");
		chamadas.add("/calcula/calcular/50/30/Caminhão caçamba/5");
	}

	private void criarResultadosEsperados() {
		resultadosEsperados = new ArrayList<>();
		resultadosEsperados.add(
				"{\"distanciaRodoviaPavimentada\": 100.0, \"distanciaRodoviaNaoPavimentada\": 0.0, \"veiculoUtilizado\": \"Caminhao cacamba\", \"pesoCarga\": 8, \"custoDoTransporte\": 62.70}");
		resultadosEsperados.add(
				"{\"distanciaRodoviaPavimentada\": 0.0, \"distanciaRodoviaNaoPavimentada\": 60.0, \"veiculoUtilizado\": \"Caminhao bau\", \"pesoCarga\": 4, \"custoDoTransporte\": 37.20}");
		resultadosEsperados.add(
				"{\"distanciaRodoviaPavimentada\": 0.0, \"distanciaRodoviaNaoPavimentada\": 180.0, \"veiculoUtilizado\": \"Carreta\", \"pesoCarga\": 12, \"custoDoTransporte\": 150.19}");
		resultadosEsperados.add(
				"{\"distanciaRodoviaPavimentada\": 80.0, \"distanciaRodoviaNaoPavimentada\": 20.0, \"veiculoUtilizado\": \"Caminhao bau\", \"pesoCarga\": 6, \"custoDoTransporte\": 57.60}");
		resultadosEsperados.add(
				"{\"distanciaRodoviaPavimentada\": 50.0, \"distanciaRodoviaNaoPavimentada\": 30.0, \"veiculoUtilizado\": \"Caminhao cacamba\", \"pesoCarga\": 5, \"custoDoTransporte\": 47.88}");
	}

	private void criaResultados() {
		resultados = new ArrayList<>();
		resultados.add(new CalculoCustoTransporte(100D, 0D, "Caminhao cacamba", 8,
				new BigDecimal(62.7).setScale(2, BigDecimal.ROUND_HALF_UP)));
		resultados.add(new CalculoCustoTransporte(0D, 60D, "Caminhao bau", 4,
				new BigDecimal(37.2).setScale(2, BigDecimal.ROUND_HALF_UP)));
		resultados.add(new CalculoCustoTransporte(0D, 180D, "Carreta", 12,
				new BigDecimal(150.19).setScale(2, BigDecimal.ROUND_HALF_UP)));
		resultados.add(new CalculoCustoTransporte(80D, 20D, "Caminhao bau", 6,
				new BigDecimal(57.6).setScale(2, BigDecimal.ROUND_HALF_UP)));
		resultados.add(new CalculoCustoTransporte(50D, 30D, "Caminhao cacamba", 5,
				new BigDecimal(47.88).setScale(2, BigDecimal.ROUND_HALF_UP)));
	}
}
