package br.com.darlanmachado.testesoftplan.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.darlanmachado.testesoftplan.domain.CalculoCustoTransporte;
import br.com.darlanmachado.testesoftplan.domain.Carga;
import br.com.darlanmachado.testesoftplan.repositories.CargaRepository;
import br.com.darlanmachado.testesoftplan.repositories.VeiculoRepository;
import br.com.darlanmachado.testesoftplan.utils.enums.CustoTransporte;

@Service
public class CalculoService {
	@Autowired
	private VeiculoRepository veiculoRepo;
	@Autowired
	private CargaRepository cargaRepo;

	private static Logger logger = Logger.getLogger(CalculoService.class.getName());

	private Carga carga;
	
	public CalculoCustoTransporte calcularCustoTransporte(CalculoCustoTransporte cct) throws Exception {
		logger.info("Iniciando processo de cálculo");
		logger.info("Dados a serem processados: " + cct);

		logger.info("Calculando distância total");
		Double distanciaTotal = calculaDistanciaTotal(cct.getDistanciaRodoviaNaoPavimentada(), cct.getDistanciaRodoviaPavimentada());
		
		logger.info("Processa valor referente a carga");
		processaValorReferenteCarga(cct.getPesoCarga());
		
		logger.info("Processa valor referente a rodovia pavimentada");
		BigDecimal subTotal = processaValorReferenteRodovia(cct.getDistanciaRodoviaPavimentada(), CustoTransporte.PAVIMENTADA.getCusto());
		logger.info("Valor rodovia pavimentada => " + subTotal);
		
		logger.info("Processa valor referente a rodovia não pavimentada");
		subTotal = subTotal.add(processaValorReferenteRodovia(cct.getDistanciaRodoviaNaoPavimentada(), CustoTransporte.NAO_PAVIMENTADA.getCusto()));
		logger.info("Valor considerando trechos de rodovia => " + subTotal);

		logger.info("Calculando valor referente ao tipo de veículo");
		subTotal = subTotal.multiply(processaValorReferenteTipoVeiculo(cct.getVeiculoUtilizado()));
		logger.info("Valor considerando tipo de veículo => " + subTotal);

		logger.info("Calculando valor referente ao peso da carga");
		subTotal = calculaConsiderandoPesoCarga(subTotal, distanciaTotal);
		logger.info("Valor considerando peso da carga => " + subTotal);
		
		logger.info("Define o valor do custo do transporte");
		cct.setCustoDoTransporte(subTotal.setScale(2, BigDecimal.ROUND_HALF_UP));

		logger.info("Finalizando o cálculo e retornando o custo => " + cct);
		return cct;
	}

	private BigDecimal calculaConsiderandoPesoCarga(BigDecimal subTotal, Double distanciaTotal) {
		if(null != getCarga()) {
			BigDecimal peso = new BigDecimal(getCarga().getPesoReferencia());
			BigDecimal referenciaCargaEspecifica = getCarga().getCustoPorKm().multiply(peso);
			BigDecimal distancias = new BigDecimal(distanciaTotal);
			subTotal = subTotal.add(referenciaCargaEspecifica.multiply(distancias));
		}
		return subTotal;
	}

	private BigDecimal processaValorReferenteTipoVeiculo(String veiculoUtilizado) {
		return veiculoRepo.findByTipoVeiculo(veiculoUtilizado).getCustoPorKm();
	}

	private BigDecimal processaValorReferenteRodovia(Double distanciaRodovia, BigDecimal custoTransporte) {
		if(0 == distanciaRodovia.intValue()) {
			return new BigDecimal(0);
		}
		BigDecimal vr = new BigDecimal(distanciaRodovia);
		return vr.multiply(custoTransporte);
	}

	private void processaValorReferenteCarga(final Integer pesoCarga) {
		List<Carga> cargas = cargaRepo.findAll();
		for(Carga c : cargas) {
			if(c.getPesoReferencia() < pesoCarga) {
				setCarga(new Carga());
				getCarga().setCustoPorKm(c.getCustoPorKm());
				getCarga().setPesoReferencia(pesoCarga - c.getPesoReferencia());
				return;
			}
		}
		setCarga(null);
	}

	private Double calculaDistanciaTotal(Double distanciaRodoviaNaoPavimentada, Double distanciaRodoviaPavimentada) {
		return distanciaRodoviaPavimentada + distanciaRodoviaNaoPavimentada;
	}
	
	public Carga getCarga() {
		return carga;
	}
	
	public void setCarga(Carga carga) {
		this.carga = carga;
	}

}
