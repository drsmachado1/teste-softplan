package br.com.darlanmachado.testesoftplan.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.darlanmachado.testesoftplan.domain.Veiculo;
import br.com.darlanmachado.testesoftplan.repositories.VeiculoRepository;

@Service
public class VeiculoService {
	@Autowired
	private VeiculoRepository repo;
	
	public Veiculo gravarVeiculo(Veiculo veiculo) throws Exception {
		return repo.save(veiculo);
	}
	
	public void excluirVeiculo(Veiculo veiculo) throws Exception {
		repo.delete(veiculo);
	}
	
	public Veiculo buscarPorId(Long id) throws Exception {
		return repo.findById(id).get();
	}
	
	public List<Veiculo> listarVeiculos() throws Exception {
		return repo.findAll();
	}
	
	public Veiculo buscarPorTipoVeiculo(String tipoVeiculo) throws Exception {
		return repo.findByTipoVeiculo(tipoVeiculo);
	}
}
