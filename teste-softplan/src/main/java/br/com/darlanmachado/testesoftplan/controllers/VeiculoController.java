package br.com.darlanmachado.testesoftplan.controllers;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.darlanmachado.testesoftplan.domain.Veiculo;
import br.com.darlanmachado.testesoftplan.services.VeiculoService;

@RestController
@RequestMapping("/veiculo")
public class VeiculoController {
	@Autowired
	private VeiculoService service;

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<List<Veiculo>> listarVeiculos() {
		ResponseEntity<List<Veiculo>> re = null;
		try {
			List<Veiculo> veiculos = service.listarVeiculos();
			re = new ResponseEntity<List<Veiculo>>(veiculos, HttpStatus.OK);
		} catch (Exception ex) {
			re = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return re;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ResponseEntity<Veiculo> gravarVeiculo(@RequestBody Veiculo veiculo) {
		ResponseEntity<Veiculo> re = null;
		try {
			Veiculo veiculo_ = service.buscarPorTipoVeiculo(veiculo.getTipoVeiculo());
			if(null != veiculo_) {
				throw new Exception("Veiculo do tipo " + veiculo.getTipoVeiculo() + " já existe!");
			}
			re = new ResponseEntity<>(service.gravarVeiculo(veiculo), HttpStatus.OK);
		} catch (Exception ex) {
			re = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return re;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Veiculo> atualizarVeiculo(@PathVariable Long id, @RequestBody Veiculo veiculo) {
		ResponseEntity<Veiculo> re = null;
		try {
			Veiculo vei = service.buscarPorId(id);
			BeanUtils.copyProperties(veiculo, vei);
			re = new ResponseEntity<>(service.gravarVeiculo(vei), HttpStatus.OK);
		} catch (Exception ex) {
			re = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return re;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> excluirVeiculo(@PathVariable Long id) {
		ResponseEntity<String> re = null;
		try {
			service.excluirVeiculo(service.buscarPorId(id));
			re = new ResponseEntity<String>("", HttpStatus.OK);
		} catch (Exception ex) {
			re = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return re;
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Veiculo> buscarVeiculoPorId(@PathVariable Long id) {
		ResponseEntity<Veiculo> re = null;
		try {
			re = new ResponseEntity<>(service.buscarPorId(id), HttpStatus.OK);
		} catch (Exception ex) {
			re = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return re;
	}
}
