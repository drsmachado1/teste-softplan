package br.com.darlanmachado.testesoftplan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.darlanmachado.testesoftplan.domain.Carga;

public interface CargaRepository extends JpaRepository<Carga, Long> {
	
}
