package br.com.darlanmachado.testesoftplan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteSoftplanApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteSoftplanApplication.class, args);
	}
}
