package br.com.darlanmachado.testesoftplan.utils.enums;

import java.math.BigDecimal;

public enum CustoTransporte {
	PAVIMENTADA(new BigDecimal(0.54D)),
	NAO_PAVIMENTADA(new BigDecimal(0.62D));
	
	private BigDecimal custo;
	
	private CustoTransporte(BigDecimal custo) {
		setCusto(custo);
	}
	
	public BigDecimal getCusto() {
		return custo;
	}
	
	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}
}
