package br.com.darlanmachado.testesoftplan.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.darlanmachado.testesoftplan.domain.Veiculo;

public interface VeiculoRepository extends JpaRepository<Veiculo, Long> {

	Veiculo findByTipoVeiculo(String tipoVeiculo);

}
