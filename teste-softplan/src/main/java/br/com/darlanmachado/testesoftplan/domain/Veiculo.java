package br.com.darlanmachado.testesoftplan.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VEICULOS")
public class Veiculo {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;
	@Column(name = "TIPO_VEICULO")
	private String tipoVeiculo;
	@Column(name = "CUSTO_POR_KM")
	private BigDecimal custoPorKm;

	public Veiculo() {
	}

	public Veiculo(Long id, String tipoVeiculo, BigDecimal custoPorKm) {
		super();
		this.id = id;
		this.tipoVeiculo = tipoVeiculo;
		this.custoPorKm = custoPorKm;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoVeiculo() {
		return tipoVeiculo;
	}

	public void setTipoVeiculo(String tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}

	public BigDecimal getCustoPorKm() {
		return custoPorKm;
	}

	public void setCustoPorKm(BigDecimal custoPorKm) {
		this.custoPorKm = custoPorKm;
	}
}
