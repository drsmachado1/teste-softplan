package br.com.darlanmachado.testesoftplan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.darlanmachado.testesoftplan.domain.CalculoCustoTransporte;
import br.com.darlanmachado.testesoftplan.services.CalculoService;

@RestController
@RequestMapping("/calcula")
public class CalculaCustoTransporteController {
	@Autowired
	private CalculoService service;

	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping(value = "/calcular/{distanciaRP}/{distanciaNRP}/{tipoVeiculo}/{pesoCarga}", method = RequestMethod.GET)
	public ResponseEntity<CalculoCustoTransporte> calcularCustoTransporte(@PathVariable(name = "distanciaRP")Double distanciaRP, 
			@PathVariable(name = "distanciaNRP") Double distanciaNRP,
			@PathVariable(name = "tipoVeiculo") String tipoVeiculo,
			@PathVariable(name = "pesoCarga") Integer pesoCarga) {

		ResponseEntity<CalculoCustoTransporte> re = null;
		try {			
			CalculoCustoTransporte cct = new CalculoCustoTransporte();
			cct.setDistanciaRodoviaNaoPavimentada(distanciaNRP);
			cct.setDistanciaRodoviaPavimentada(distanciaRP);
			cct.setPesoCarga(pesoCarga);
			cct.setVeiculoUtilizado(tipoVeiculo);
			re = new ResponseEntity<CalculoCustoTransporte>(service.calcularCustoTransporte(cct), HttpStatus.OK);
		} catch (Exception ex) {
			re = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return re;
	}
}
