package br.com.darlanmachado.testesoftplan.domain;

import java.math.BigDecimal;

public class CalculoCustoTransporte {
	private Double distanciaRodoviaPavimentada;
	private Double distanciaRodoviaNaoPavimentada;
	private String veiculoUtilizado;
	private Integer pesoCarga;
	private BigDecimal custoDoTransporte;
	
	public CalculoCustoTransporte() {}
	
	public CalculoCustoTransporte(Double distanciaRodoviaPavimentada, Double distanciaRodoviaNaoPavimentada,
			String veiculoUtilizado, Integer pesoCarga, BigDecimal custoDoTransporte) {
		super();
		this.distanciaRodoviaPavimentada = distanciaRodoviaPavimentada;
		this.distanciaRodoviaNaoPavimentada = distanciaRodoviaNaoPavimentada;
		this.veiculoUtilizado = veiculoUtilizado;
		this.pesoCarga = pesoCarga;
		this.custoDoTransporte = custoDoTransporte;
	}

	public Double getDistanciaRodoviaPavimentada() {
		return distanciaRodoviaPavimentada;
	}

	public void setDistanciaRodoviaPavimentada(Double distanciaRodoviaPavimentada) {
		this.distanciaRodoviaPavimentada = distanciaRodoviaPavimentada;
	}

	public Double getDistanciaRodoviaNaoPavimentada() {
		return distanciaRodoviaNaoPavimentada;
	}

	public void setDistanciaRodoviaNaoPavimentada(Double distanciaRodoviaNaoPavimentada) {
		this.distanciaRodoviaNaoPavimentada = distanciaRodoviaNaoPavimentada;
	}

	public String getVeiculoUtilizado() {
		return veiculoUtilizado;
	}

	public void setVeiculoUtilizado(String veiculoUtilizado) {
		this.veiculoUtilizado = veiculoUtilizado;
	}

	public Integer getPesoCarga() {
		return pesoCarga;
	}

	public void setPesoCarga(Integer pesoCarga) {
		this.pesoCarga = pesoCarga;
	}

	public BigDecimal getCustoDoTransporte() {
		return custoDoTransporte;
	}

	public void setCustoDoTransporte(BigDecimal custoDoTransporte) {
		this.custoDoTransporte = custoDoTransporte;
	}
	
	@Override
	public String toString() {
		return "Dados para cálculo: " +
				"\nDistância rodovia pavimentada => " + distanciaRodoviaPavimentada +
				"\nDistância rodovia não pavimentada => " + distanciaRodoviaNaoPavimentada +
				"\nVeiculo utilizado => " + veiculoUtilizado +
				"\nPeso da carga => " + pesoCarga +
				"\nCusto do transporte => " + (null == custoDoTransporte ? "Não calculado" : custoDoTransporte.setScale(2, BigDecimal.ROUND_HALF_UP));
	}
}
