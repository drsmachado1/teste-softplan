package br.com.darlanmachado.testesoftplan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.darlanmachado.testesoftplan.domain.Carga;
import br.com.darlanmachado.testesoftplan.repositories.CargaRepository;

@RestController
@RequestMapping("/carga")
public class CargaController {
	@Autowired
	private CargaRepository repo;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<List<Carga>> listarCargas() {
		ResponseEntity<List<Carga>> re = null;
		try {
			List<Carga> cargas = repo.findAll();
			re = new ResponseEntity<List<Carga>>(cargas, HttpStatus.OK);
		} catch (Exception ex) {
			re = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return re;
	}
}
