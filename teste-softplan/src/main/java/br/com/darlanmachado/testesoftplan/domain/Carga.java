package br.com.darlanmachado.testesoftplan.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CARGAS")
public class Carga {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;
	@Column(name = "PESO_REFEENCIA")
	private Integer pesoReferencia;
	@Column(name = "CUSTO_POR_KM")
	private BigDecimal custoPorKm;

	public Carga() {}
	
	public Carga(Long id, Integer pesoReferencia, BigDecimal custoPorKm) {
		super();
		this.id = id;
		this.pesoReferencia = pesoReferencia;
		this.custoPorKm = custoPorKm;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPesoReferencia() {
		return pesoReferencia;
	}

	public void setPesoReferencia(Integer pesoReferencia) {
		this.pesoReferencia = pesoReferencia;
	}

	public BigDecimal getCustoPorKm() {
		return custoPorKm;
	}

	public void setCustoPorKm(BigDecimal custoPorKm) {
		this.custoPorKm = custoPorKm;
	}
}
