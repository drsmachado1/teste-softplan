CREATE TABLE IF NOT EXISTS VEICULOS (
	ID BIGINT AUTO_INCREMENT PRIMARY KEY,
	TIPO_VEICULO VARCHAR(30) NOT NULL,
	CUSTO_POR_KM FLOAT(2) NOT NULL
);

CREATE TABLE IF NOT EXISTS CARGAS (
	ID BIGINT AUTO_INCREMENT PRIMARY KEY,
	PESO_REFEENCIA INT NOT NULL,
	CUSTO_POR_KM FLOAT(2) NOT NULL
);